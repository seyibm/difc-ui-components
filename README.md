##How to setup DIFC UI Components on the machine (works best on a Mac)

1. Open the terminal and copy/paste `git clone https://seyibm@bitbucket.org/seyibm/difc-ui-components.git`
2. Enter the project folder and do `npm install` to install all dependecies
3. Then do `npm run serve` to run project (a browser window should open) and leave that running.
4. Open another terminal window, and do `npm run storybook` to run Storybook (another browser window should open)
5. Enjoy Storybook.
6. To interact with some of the UI components go to `http://localhost:8080/components.html`, as storybook is just a display of UI states with no interaction. (edited)

Remote url: `https://seyibm@bitbucket.org/seyibm/difc-ui-components.git`
